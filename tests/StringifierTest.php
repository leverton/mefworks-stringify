<?php namespace mef\UnitTest\Text;

use StdClass;
use DateTimeImmutable;
use mef\Stringifier\Stringifier;

class StringifierTest extends \PHPUnit_Framework_TestCase
{
	public function testNull()
	{
		$stringifier = new Stringifier;

		$this->assertEquals('NULL', $stringifier->stringify(null));
	}

	public function testString()
	{
		$stringifier = new Stringifier;

		$text = 'Hello, World!';
		$this->assertEquals($text, $stringifier->stringify($text));
	}

	public function testArray()
	{
		$stringifier = new Stringifier;

		$data = ['a', 'b', 'c'];

		$this->assertTrue(is_string($stringifier->stringify($data)));
	}

	public function testTraversable()
	{
		$stringifier = new Stringifier;

		$data = new \ArrayIterator(['a', 'b', 'c']);

		$this->assertTrue(is_string($stringifier->stringify($data)));
	}

	public function testIterationLimit()
	{
		$stringifier = new Stringifier;

		$stringifier->setIterationLimit(0);
		$this->assertEquals(0, $stringifier->getIterationLimit(0));

		$data = ['a', 'b', 'c'];

		$this->assertSame('[]', $stringifier->stringify($data));

		$stringifier->setIterationLimit(2);

		$this->assertTrue(is_string($stringifier->stringify($data)));
	}

	public function testResource()
	{
		$stringifier = new Stringifier;

		$fp = fopen('php://stdout', 'w');

		$this->assertTrue(is_string($stringifier->stringify($fp)));

		$this->assertEquals('resource', $stringifier->stringify($fp));

		fclose($fp);
	}

	public function testDateTimeInterface()
	{
		$stringifier = new Stringifier;

		$dt = new DateTimeImmutable;
		$this->assertTrue(is_string($stringifier->stringify($dt)));
	}

	public function testToString()
	{
		$stringifier = new Stringifier;

		$stringObject = new StringObject;
		$this->assertEquals((string) $stringObject, $stringifier->stringify($stringObject));
	}

	public function testCallWithoutToString()
	{
		$stringifier = new Stringifier;

		$callObject = new CallObject;
		$this->assertTrue(is_string($stringifier->stringify($callObject)));
	}

	public function testObject()
	{
		$stringifier = new Stringifier;

		$object = new SimpleObject;
		$this->assertTrue(is_string($stringifier->stringify($object)));
	}

	public function testInteger()
	{
		$stringifier = new Stringifier;

		$this->assertTrue(is_string($stringifier->stringify(42)));
	}

	public function testCycle()
	{
		$a = new StdClass;
		$b = new StdClass;

		$a->b = $b;
		$b->a = $a;

		$stringifier = new Stringifier;
		$this->assertTrue(is_string($stringifier->stringify($a)));

		$a = new StdClass;
		$a->foo = 'bar';

		$string = $stringifier->stringify([$a, $a]);

		$this->assertSame(2, substr_count($string, 'bar'));
	}
}

class CallObject
{
	public function __call($method, $args)
	{
	}
}

class StringObject
{
	public function __toString()
	{
		return 'StringObject';
	}
}

class SimpleObject
{
	public $public = 'public';
	protected $protected = 'protected';
	private $private = 'private';
}