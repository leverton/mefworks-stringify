<?php namespace mef\Stringifier\Example;

require __DIR__ . '/../vendor/autoload.php';

$stringifier = new \mef\Stringifier\Stringifier;


// The stringifier will turn any object into a string. Some implementatoins may
// raise an exception, but the return value will always be a string.
// The exact output shouldn't be relied upon to be the same. The only guarantee
// is that it will be a string that somehow faithfully represents the value
// passed to it.

echo $stringifier->stringify(new \DateTime), PHP_EOL;

echo $stringifier->stringify(['a', 'b', 'c']), PHP_EOL;

echo $stringifier->stringify(['foo' => 'bar']), PHP_EOL;

echo $stringifier->stringify(new \ArrayIterator(['foo' => 'bar'])), PHP_EOL;

// Cycles are detected

$a = new \StdClass;
$b = new \StdClass;
$a->value = 42;
$a->b = $b;
$b->value = 666;
$b->a = $a;

echo $stringifier->stringify($a), PHP_EOL;