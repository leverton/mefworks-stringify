<?php namespace mef\Stringifier;

/**
 * A stringifier that uses the native PHP json_encode.
 *
 * It has a special case for resources.
 */
class JsonStringifier implements StringifierInterface
{
	const DEFAULT_DEPTH = 512;

	/**
	 * @var integer
	 */
	protected $options;

	/**
	 * @var integer
	 */
	protected $depth;

	/**
	 * Constructor
	 *
	 * See http://php.net/json_encode for information on the options.
	 *
	 * @param integer $options a bitmask of valid JSON constants
	 * @param integer $depth   the maximum recursion depth
	 */
	public function __construct($options = 0, $depth = self::DEFAULT_DEPTH)
	{
		$this->options = (int) $options;
		$this->depth = (int) $depth;
	}

	/**
	 * Return the current options bitmask.
	 *
	 * See http://php.net/json_encode for information on the options.
	 *
	 * @return integer
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * Set options bitmask.
	 *
	 * See http://php.net/json_encode for information on the options.
	 *
	 * @param integer $options
	 */
	public function setOptions($options)
	{
		$this->options = (int) $options;
	}

	/**
	 * Get the maximum resursion depth
	 *
	 * @return integer
	 */
	public function getDepth()
	{
		return $this->depth;
	}

	/**
	 * Set the maximum recursion depth
	 *
	 * @param integer $depth
	 */
	public function setDepth($depth)
	{
		$this->depth = (int) $depth;
	}

	/**
	 * Return a JSON encoded string.
	 *
	 * @param mixed $value
	 *
	 * @return string
	 */
	public function stringify($value)
	{
		return is_resource($value) === false ?
			(json_encode($value, $this->options, $this->depth) ?: '') :
			'"resource"';
	}
}