<?php namespace mef\Stringifier;

/**
 * A stringifier that uses the native PHP var_dump.
 */
class VarDumpStringifier implements StringifierInterface
{
	/**
	 * Capture the output of var_dump and return it as a string.
	 *
	 * @param mixed $value
	 *
	 * @return string
	 */
	public function stringify($value)
	{
		ob_start();
		var_dump($value);
		return ob_get_clean();
	}
}
