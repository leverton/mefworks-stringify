<?php namespace mef\Stringifier;

use DateTimeInterface;
use SplObjectStorage;
use Traversable;

/**
 * Convert any variable into a string.
 *
 * The only intended purpose of this class is to assist in debugging,
 * particularly with logging to a file.
 *
 * One should not rely on the output of stringify() to be in any particular
 * format. The only guarantee is that it returns a string.
 */
class Stringifier implements StringifierInterface
{
	private $visitedObjects;

	/**
	 * The maximum number of iterations when iterating over an array.
	 *
	 * @var integer
	 */
	private $iterationLimit = 50;

	public function __construct()
	{
		$this->visitedObjects = new SplObjectStorage;
	}

	/**
	 * Return the iteration limit.
	 *
	 * @return integer
	 */
	final public function getIterationLimit()
	{
		return $this->iterationLimit;
	}

	/**
	 * Set the iteration limit.
	 *
	 * @param integer $limit
	 */
	final public function setIterationLimit($limit)
	{
		$this->iterationLimit = (int) $limit;
	}

	/**
	 * Return a string representation of the value.
	 *
	 * @param mixed $value
	 *
	 * @return string
	 */
	public function stringify($value)
	{
		if (($is_object = is_object($value)) === true)
		{
			if ($this->visitedObjects->contains($value))
			{
				return '*cycle*';
			}
			else
			{
				$this->visitedObjects->attach($value);
			}
		}

		$string = '';

		if ($value === null)
		{
			$string = 'NULL';
		}
		else if (is_scalar($value))
		{
			$string = (string) $value;
		}
		else if (is_array($value))
		{
			$indexedArray = isset($value[0]) && isset($value[count($value) - 1]);

			if ($indexedArray === true)
			{
				$string = $this->iterate($value, '[', ']', false);
			}
			else
			{
				$string = $this->iterate($value, '{', '}', true);
			}
		}
		else if ($value instanceof Traversable)
		{
			$string = $this->iterate($value, get_class($value) . '<', '>', true);
		}
		else if (is_resource($value))
		{
			$string = 'resource';
		}
		else if ($value instanceof DateTimeInterface)
		{
			$string = $value->format('Y-m-d H:i:s');
		}
		else if ($is_object === true)
		{
			if (method_exists($value, '__toString'))
			{
				$string = (string) $value;
			}
			else
			{
				$string = get_class($value) . $this->stringify(get_object_vars($value));
			}
		}

		if ($is_object === true)
		{
			$this->visitedObjects->detach($value);
		}

		return $string;
	}

	/**
	 * Iterate over a traversable object, creating string representations for
	 * each while separating them by a comma.
	 *
	 * @param  array|Traversable $object
	 * @param  string  $openChar  a string to prepend to the list
	 * @param  string  $closeChar a string to append to the list
	 * @param  boolean $useKeys   if true, show the keys
	 *
	 * @return string
	 */
	private function iterate($object, $openChar, $closeChar, $useKeys)
	{
		$limit = $this->iterationLimit;

		$string = $openChar;

		$first = true;
		foreach ($object as $key => $value)
		{
			if ($limit-- === 0)
			{
				break;
			}

			if ($first === true)
			{
				$first = false;
			}
			else
			{
				$string .= ', ';
			}

			if ($useKeys === true)
			{
				$string .= $this->stringify($key) . ': ';
			}

			$string .= $this->stringify($value);
		}

		$string .= $closeChar;

		return $string;
	}
}