<?php namespace mef\Stringifier;

interface StringifierInterface
{
	/**
	 * Returns a string representation of the value.
	 *
	 * @param mixed $value
	 *
	 * @return string
	 */
	public function stringify($value);
}