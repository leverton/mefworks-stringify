<?php namespace mef\Stringifier;

/**
 * A stringifier that uses the native PHP print_r.
 */
class PrintRStringifier implements StringifierInterface
{
	/**
	 * Return the output of print_r
	 *
	 * @param mixed $value
	 *
	 * @return string
	 */
	public function stringify($value)
	{
		return print_r($value, true);
	}
}
