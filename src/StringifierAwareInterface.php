<?php namespace mef\Stringifier;

use mef\Stringifier\StringifierInterface;

interface StringifierAwareInterface
{
	/**
	 * Set the stringifier
	 *
	 * @param \mef\Stringifier\StringifierInterface $stringifier
	 */
	public function setStringifier(StringifierInterface $stringifier);

	/**
	 * Return the stringifier
	 *
	 * @return \mef\Stringifier\StringifierInterface
	 */
	public function getStringifier();
}