mef\Stringifier - Turn any object into a string
===============================================

[![Total Downloads](https://poser.pugx.org/mefworks/stringifier/downloads.png)](https://packagist.org/packages/mefworks/stringifier)
[![Latest Stable Version](https://poser.pugx.org/mefworks/stringifier/v/stable.png)](https://packagist.org/packages/mefworks/stringifier)

mef\Stringifier defines an interface `mef\StringifierInterface` that describes
a way to turn any PHP value (including objects, resources, arrays, etc) into a
string. The purpose of this is primarily to assist in logging and debugging.

Examples
--------

*Standard stringifier*


```php
<?php
$stringifier = new \mef\Stringifier\Stringifier;

echo $stringifier->stringify(new \DateTime), PHP_EOL;
echo $stringifier->stringify(['a', 'b', 'c']), PHP_EOL;
echo $stringifier->stringify(['foo' => 'bar']), PHP_EOL;
echo $stringifier->stringify(new \ArrayIterator(['foo' => 'bar'])), PHP_EOL;
```

Sample Output:

	2014-09-17 15:58:26
	[a, b, c]
	{foo: bar}
	ArrayIterator<foo: bar>

Note that the actual output may vary as the implementation details are not
guaranteed to be the same from version to version. Of course, it will always
return a string that faithfully represents the value.

More complete examples are available in the `examples` directory.

Overview
--------
The `mef\StringifierInterface` only describes one function:

```php
<?php
namespace mef\Stringifier;

interface StringifierInterface
{
	/**
	 * Returns a string representation of the value.
	 *
	 * @param mixed $value
	 *
	 * @return string
	 */
	public function stringify($value);
}
```

Any conforming stringifier will *always* return a string, unless it throws an
exception. There are no pre-defined exceptions in the `mef\Stringifier`
namespace.

Stringifiers
------------

* `Stringifier` - Good for most cases. Tries to return a reasonable string
representation of any value.
* `JsonStringifier` - A thin wrapper over `json_encode`. Returns a JSON encoded
representation of the value.
* `VarDumpStringifier` - Returns the results of a `var_dump`.
* `PrintRStringifier` - Returns the results of a `print_r`.

License
=======

Copyright (C) 2014 Matthew Leverton

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.